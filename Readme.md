﻿# Learn Django
![enter image description here](https://static.djangoproject.com/img/logos/django-logo-negative.svg)

Repositori  / Project ini merupakan sarana saya dalam mempelajari django *framework* berbasis python. Dokumen ini berisi cara dan sintaks yang digunakan untuk membuat website menggunakan django *framework*.

## Table of Contents
1. [How To Install](#how-to-install)
2. [Set Directory Project](#set-directory-project)
3. [Create an App](#create-an-app)
4. [Create URL](#create-url)
5. [Create Views](#create-views)


## How To Install
Pastikan PC / Laptop sudah terinstall python versi terbaru. Pada repo ini, saya menggunakan **Windows 10** sebagai *operating system* nya.

Setelah python terinstall, jalankan perintah berikut pada **windows powershell** atau **command prompt**

```
pip install virtualenvwrapper-win
```
Setelah itu, buat *virtual environtment* untuk membuat sebuah *project*
```
mkvirtualenv <nama_env>
```
<nama_env> silakan disesuaikan. **misal:  webdesign**

setelah *virtual env* dibuat, untuk menjalankan nya gunakan perintah berikut
```
workon <nama_env>
```
Setelah *virtual env* berjalan, selanjutnya adalah menginstall *django*
```
pip install django
```
Gunakan perintah berikut untuk memeriksa versi dari *django* yang telah terinstall
```
python -m django --version
```
[Back to Top](#table-of-contents)

## Set Directory Project

Setelah *django* terinstal, selanjutnya adalah membuat direktori *prorject*. Pastikan **windows powershell** atau **command prompt** masih terbuka

Pada repo ini saya menggunakan C:/ sebagai direktori projek dengan mengetik perintah berikut.
```
cd \
```
setelah itu buatlah sebuah direktori baru. **misal: newdir**
```
cd newdir
```
setelah itu, buat *project django* dengan menjalankan perintah 
```
django-admin startproject <nama_project>
```
<nama_project> disesuaikan dengan <nama_env>. 
Setelah selesai, masuk ke dalam direktori  <nama_project>
```
cd <nama_project>
```
Kemudian untuk me-*running* *server* jalankan perintah
```
python manage.py runserver
```
[Back to Top](#table-of-contents)

## Create an App

Setelah membuat *project*, saatnya untuk membuat *apps*, yaitu *file-file* untuk membuat halaman website kita.

Masih pada direktori yang sama, yaitu **C:/newdir/webdesign** jalankan perintah berikut.

    python manage.py startapp <nama_app>

<nama_app> disini saya menggunakan nama **coba**

Jika sudah selesai, akan ada folder **coba** pada root (folder **webdesign**). Tambahkan apps yang baru saja dibuat pada file **settings.py**

    INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',    
    'django.contrib.staticfiles',
    'coba', #Apps coba
    )

[Back to Top](#table-of-contents)

## Create URL
Setelah apps ditambahkan, saatnya membuat URL pada apps tersebut (seperti *routing* pada laravel)

Buka file **urls.py** kemudian tambahkan perintah berikut

    from <nama_apps> import views
note: <nama_apps> pada repo ini adalah **coba**

kemudian pada urlpatterns=[], tambahkan perintah berikut

    urlpatterns = [
	    path('admin/', admin.site.urls),
	    path('', views.index, name='index'), #Tambahkan baris ini
    ]

[Back to Top](#table-of-contents)

## Create Views
Setelah mendefinisikan url, selanjutnya adalah membuat *view*. silakan buka **views.py** yang ada pada direktori apps (pada repo ini adalah  folder **coba**)

buatlah sebuah fungsi bernama index dengan parameter *request* seperti berikut.

    from django.shortcuts import render
    
    #Tambahkan Fungsi Berikut.
    def  index(request):
	    return render(request, 'webdesign/index.html')

Setelah itu, pada root, buatlah folder **templates** yang berisi folder **webdesign**. Kemudian isi folder **webdesign** dengan file *index.html*. Pada file inilah kita akan membuat desain web.

Setelah membuat *file index.html*, selanjutnya buka *file* **settings.py** kemudian pada bagian array **TEMPLATES** isi array **DIR** dengan direktori templates sebagai berikut.

    TEMPLATES = [
        {
		    'BACKEND': 'django.template.backends.django.DjangoTemplates',
		    'DIRS': ['./templates'], #Tambahkan Ini
		    'APP_DIRS': True,
		    'OPTIONS': {
			    'context_processors': [
			    'django.template.context_processors.debug',
			    'django.template.context_processors.request',
			    'django.contrib.auth.context_processors.auth',
			    'django.contrib.messages.context_processors.messages',
			    ],
		    },
	    },
    ]
    
Setelah selesai, jalankan

    python manage.py runserver

[Back to Top](#table-of-contents)
