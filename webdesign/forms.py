from django.forms import ModelForm
from coba.models import Song

class SongForm(ModelForm):

    class Meta:
        model = Song
        fields = ['title', 'genre', 'singer', 'rating']